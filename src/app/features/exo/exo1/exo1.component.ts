import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-exo1',
  templateUrl: './exo1.component.html',
  styleUrls: ['./exo1.component.scss']
})
export class Exo1Component implements OnInit {

  milliseconds: number;

  memory: number;

  timer: any;

  constructor() { }

  ngOnInit(): void {
    this.milliseconds = 0;
    this.memory = 0;
  }

  start() {
    if(!this.timer) {
      let startTime = new Date().getTime();
      this.timer = setInterval(() => {
        let current = new Date().getTime();
        this.milliseconds = this.memory + current - startTime;
      }, 1);
    }
  }

  stop() {
    if(this.timer) {
      clearInterval(this.timer);
      this.timer = null;
    }
    this.memory = this.milliseconds;
  }

  reset() {
    this.stop();
    this.memory = 0;
    this.milliseconds =0;
  }

}
