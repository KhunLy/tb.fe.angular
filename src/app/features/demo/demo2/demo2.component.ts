import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo2',
  templateUrl: './demo2.component.html',
  styleUrls: ['./demo2.component.scss']
})
export class Demo2Component implements OnInit {

  someString : string;

  compt: number;


  constructor() { }

  ngOnInit(): void {
    this.someString = 'world' ;
    this.compt = 0;
  }

  changeText() {
    this.someString = 'Khun';
  }

  back() {
    this.someString = 'world'
  }

  increase() {
    this.compt++; 
  }

  decrease() {
    this.compt--;
  }

}
