import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo1',
  templateUrl: './demo1.component.html',
  styleUrls: ['./demo1.component.scss']
})
export class Demo1Component implements OnInit {

  someString: string;

  someNumber: number;

  someDate: Date;

  someBoolean: boolean;

  constructor() { }

  ngOnInit(): void {
    this.someString = 'WoRlD';
    this.someNumber = 42.52666666;
    this.someDate = new Date('2000-01-31');
    this.someBoolean = true;
  }
}