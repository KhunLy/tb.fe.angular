import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output()
  menuClicked: EventEmitter<any>

  constructor() { 
    this.menuClicked = new EventEmitter();
  }

  ngOnInit(): void {

  }

  menu() {
    this.menuClicked.emit();
  }
}
