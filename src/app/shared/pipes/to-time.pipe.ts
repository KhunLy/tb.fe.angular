import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toTime'
})
export class ToTimePipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): string {
    let milliseconds = value % 1000;
    let seconds = Math.floor(value / 1000) % 60;
    let minutes = Math.floor(value / 60000) % 60;
    let hours = Math.floor(value / 3600000);
    return `${('0' + hours).slice(-2)}:${('0' + minutes).slice(-2)}:${('0' + seconds).slice(-2)}:${('00' + milliseconds).slice(-3)}`; 
  }

}
