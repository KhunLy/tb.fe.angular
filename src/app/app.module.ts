import { BrowserModule } from '@angular/platform-browser';
import { DEFAULT_CURRENCY_CODE, LOCALE_ID, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavComponent } from './shared/components/nav/nav.component';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './shared/components/header/header.component';
import { FooterComponent } from './shared/components/footer/footer.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HomeComponent } from './features/home/home.component';
import { AboutComponent } from './features/about/about.component';
import { Route } from '@angular/router';
import { DemoComponent } from './features/demo/demo.component';
import { Demo1Component } from './features/demo/demo1/demo1.component';
import { Demo2Component } from './features/demo/demo2/demo2.component';
import localeFr from '@angular/common/locales/fr';
import { registerLocaleData } from '@angular/common';
import { ExoComponent } from './features/exo/exo.component';
import { Exo1Component } from './features/exo/exo1/exo1.component';
import { ToTimePipe } from './shared/pipes/to-time.pipe';
import { Demo3Component } from './features/demo/demo3/demo3.component';

registerLocaleData(localeFr);

const routes: Route[] = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'demo', component: DemoComponent, children: [
    { path: '', redirectTo: '/demo/demo1', pathMatch: 'full' },
    { path: 'demo1', component: Demo1Component },
    { path: 'demo2', component: Demo2Component },
  ] },
  { path: 'exo', component: ExoComponent, children: [
    { path: '', redirectTo: '/exo/exo1', pathMatch: 'full' },
    { path: 'exo1', component: Exo1Component },
  ] }
]

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    AboutComponent,
    DemoComponent,
    Demo1Component,
    Demo2Component,
    ExoComponent,
    Exo1Component,
    ToTimePipe,
    Demo3Component,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FontAwesomeModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'fr-BE' },
    { provide: DEFAULT_CURRENCY_CODE, useValue: 'EUR' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
